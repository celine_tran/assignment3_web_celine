/**
 * @author Celine Tran 
 */
"use strict";
document.addEventListener("DOMContentLoaded", setup);
//placed these as global variables to prevent declaration repetitions inside of displayquote and dealwithneterror
const text = document.querySelector('#quote');
const textError = document.querySelector('#errorOccur');
//global variable for index that will be used inside of changeimg function
let index = 0;
//This will create an array filled with pictures.
const imgArr = new Array();
imgArr[0] = "img/angry.jpg"
imgArr[1] = "img/disgusted.jpg"
imgArr[2] = "img/sad.jpg"
imgArr[3] = "img/thinking.jpg"
imgArr[4] = "img/touchingStach.jpg"
imgArr[5] = "img/frowning.jpg"
imgArr[6] = "img/combing.jpg"


/**
 * This function will call the changeImg and getQuote function whenever the button is clicked
 */
function setup() {
    const btn = document.querySelector("button");
    btn.addEventListener("click", () => {
        changeImg();
        getQuote();
    });
}

/**
 * This function will each picture as a background by going through them repeatedly as the user clicks the button.
 * @param {Array} imgArr is the array of image received from the setup function.
 */
function changeImg() {
    const main = document.querySelector('#main');
    if (index > imgArr.length - 1) {
        index = 0;
    }
    let url = 'url(' + imgArr[index] + ')';
    main.style.backgroundImage = url;
    index = index + 1;

}

/**
 * This function uses fetch and promise to get the quotes from an api. It convert it to json if the response is 200ok, then will call displayQuote with passed in quote.
 * It will throw an exception if that is not the case, then will call dealwitherror function and pass in the error 
 * @param {MouseEvent} e The event passed by event clicked in setup function
 * @returns {Promise} The Promise object,containing the quote, is converted into jason
 * 
 */
function getQuote() {
    let promise = 'https://ron-swanson-quotes.herokuapp.com/v2/quotes';
    fetch(promise)
        .then(response => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error(`Server could not respond. Status code: ${response.status}`);
            }
        })
        .then(quote => displayQuote(quote))
        .catch(error => dealWithNetError(error));
}

/**
 * This function will display the quote that was passed in by putting it inside of the heading1 where the quote should be.
 * It first checks if there has previously been an error,If the error was fixed, it will empty/remove that error paragraph before displaying new quote.
 * @param {Array} quote This is the quote passed in by getQuote function 
 */
function displayQuote(quote) {
    if (textError.textContent != '') {
        textError.textContent = '';
    }
    text.textContent = quote[0];
}

/**
 * This function will empty the quote inside of the quote box and use the passed in error to display the problem and ask user to try again.
 * @param {Error} error The error passed in from getQuote when there is a problem.
 */
function dealWithNetError(error) {
    text.textContent = '';
    textError.textContent = error + ' Try Again.';
}